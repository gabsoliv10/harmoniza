<?php require_once('../components/header.php'); ?>

<main class="page__home" role="main">
	
	<section class="main__banner">
		<ul class="owl-carousel owl-theme main__carousel">
			<li>
				<img src="../assets/images/home/banner-home.png" title="" alt="">
			</li>

			<li>
				<img src="../assets/images/home/banner-home.png" title="" alt="">
			</li>

			<li>
				<img src="../assets/images/home/banner-home.png" title="" alt="">
			</li>
		</ul>
	</section>

	<section class="block__specialty block__section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-12">

					<header>
						<h2 class="section__title">Especialidades</h2>
						<p class="section__description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
					</header>	

					<div class="specialty__listing">

						<div class="specialty__box specialty__box--img">
							<h2 class="specialty__title specialty__title--small">Dermatologista</h2>
							<p class="specialty__name">Drª Camila Nemoto</p>
						</div>

						<div class="specialty__box">
							<h2 class="specialty__title specialty__title--small">Endocrinologista</h2>
							<p class="specialty__name">Drª Mariana Valente</p>
						</div>
					
					</div>

				</div>

				<div class="col-md-4 col-xs-12 col-md-offset-2">
					<div class="book__section">

						<header>
							<h2 class="book__title"><span>Agende</span> sua consulta</h2>
							<p class="book__description">Entre em contato com seu e-mail e telefone para que possamos ajudá-lo a marcar o melhor horário para você.</p>
						</header>

						<form name="contactForm" class="book__form contact__form" method="POST">
							<div class="form__fields">
								<label for="nome" aria-labelledby="nome">
									<input type="text" name="nome" placeholder="Nome">
								</label>

								<label for="email" aria-labelledby="email">
									<input type="email" name="email" placeholder="E-mail">
								</label>

								<label for="telefone" aria-labelledby="telefone">
									<input type="tel" name="telefone" placeholder="Telefone">
								</label>

								<div class="form__actions">
									<input class="btn btn-submit" type="submit" value="Enviar">
								</div>
							</div>	
						</form>

						<header>
							<h2 class="book__title"><span>Horário</span> de atendimento</h2>
						</header>
						
						<div class="book__infos">
							<ul>
								<li><span>Segunda à sexta</span></li>

								<li>08:00 às 12:00  |  14:00 às 17:00</li>

								<li>
									<i class="fa fa-phone fa-flip-horizontal"></i>
									<a hrel="tel:+5548991045122">(48) 991045122</a>
								</li>

								<li><i class="fa fa-map-marker"></i>Av. de exemplo número 777, Florianópolis - SC</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="block__blog block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<header>
						<h2 class="section__title">Blog</h2>
						<p class="section__description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
					</header>	

					<div class="blog__listing">
						<div class="row">
							<?php for ($i=1; $i <= 2; $i++): ?>
								<div class="col-xs-12 col-md-6">

									<article class="blog__post">
										<div class="post__info" >
											<a class="post__thumbnail" href="blog_interna.php" title=""><img src="../assets/images/home/blog-home.png" title="" alt=""></a>
										</div>

										<div class="post__details">
											<a href="blog_interna.php" title="Leia mais" >
												<h3 class="post__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh</h3>
												<div class="post__excerpt">
													<p>
													Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat...
													</p>
												</div>
											</a>

											<div class="section__actions post__actions">
												<a href="blog_interna.php" title="Leia mais" class="btn-read-more">
													Continuar lendo...
												</a>
											</div>
										</div>
									</article>
								</div>
							<?php endfor; ?>
						</div>						
					</div>

					<div class="section__actions">
						<a class="btn-primary" href="#">Ver outros posts</a>
					</div>

				</div>
			</div>
		</div>
	</section>

	<?php require_once('../components/newsletter.php') ?>

	<section class="block__localizacao block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<header>
						<h2 class="section__title">Localização</h2>
					</header>	

					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="localizacao__mapa">
								<img src="../assets/images/home/mapa.png" title="" alt="">
							</div>
						</div>

						<div class="col-md-5 col-md-offset-1 col-xs-12">
							<div class="localizacao__parking">
								<img src="../assets/images/home/icon-car.png" title="" alt="" />
								<h2 class="localizacao__title">Onde <span>Estacionar?</span></h2>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>

</main>

<?php require_once('../components/footer.php') ?>