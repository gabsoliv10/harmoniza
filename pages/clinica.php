<?php require_once('../components/header-interna.php'); ?>

<main class="page__clinica" role="main">

	<section class="block__clinica">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">

					<h2 class="section__title">A Clínica</h2>
					<p class="section__description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>

					<div class="block__content">
						<p>
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
						</p>
					</div>

				</div>

				<div class="col-xs-12 col-sm-12 col-md-6">

					<div class="clinica__image">
						<img title="" src="../assets/images/clinica/image-01.png" alt="">
					</div>

				</div>
			</div>
		</div>
	</section>

	<section class="block__team">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h2 class="section__title">Nossas Especialistas</h2>
					<p class="section__description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>

					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="specialty__box specialty__box--img">
								<h2 class="specialty__title">Dermatologista</h2>
								<p class="specialty__name">Drª Camila Nemoto</p>
							</div>
						</div>
						
						<div class="col-md-6 col-xs-12">
							<div class="specialty__box">
								<h2 class="specialty__title">Endocrinologista</h2>
								<p class="specialty__name">Drª Mariana Valente</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	</main>

	<?php require_once('../components/footer.php'); ?>