<?php require_once('../components/header.php'); ?>

<main class="page__localizacao" role="main">

	<section class="block__localizacao">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-12">
					<h2 class="section__title">Localização</h2>
					<div class="localizacao__mapa">
						<img src="../assets/images/home/mapa.png" title="" alt="">
						</div>
					</div>

					<div class="col-md-5 col-md-offset-1 col-xs-12">
						<div class="localizacao__parking">
							<img src="../assets/images/home/icon-car.png" title="" alt="" />
							<h2 class="localizacao__title">Onde <span>Estacionar?</span></h2>
							
						</div>

						<div class="localizacao__infos">
							<div class="localizacao__infos-item">
								<i class="fa fa-map-marker fa-flip-horizontal"></i>
								<p>
									Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
								</p>
							</div>

							<div class="localizacao__infos-item">
								<i class="fa fa-phone fa-flip-horizontal"></i>
								<a href="tel:+5548991045122">
									48-991045122
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	</main>

	<?php require_once('../components/footer.php'); ?>