<?php require_once('../components/header-interna.php'); ?>

<main class="page__especialidades" role="main">

	<section class="block__especialidades">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<h2 class="section__title">Especialidades</h2>
					<p class="section__description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>

					<div class="specialty__box specialty__box--img interna">
						<h2 class="specialty__title">Dermatologista</h2>

						<div class="especialidades__info">
							<p class="specialty__name">Drª Camila Nemoto</p>
							<p class="specialty__cremesc">CREMESC 15880/ RQE 12052</p>
						</div>

						<p class="especialidades__text">
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
						</p>

						<p class="specialty__name mb-30">Formação Acadêmica</p>
						<p class="specialty__school">MEDICINA - UFSC</p>
						<p class="specialty__school">PÓS-GRADUAÇÃO EM DERMATOLOGIA - UFSC</p>
					</div>

					<div class="specialty__box specialty__box--img interna">
						<h2 class="specialty__title">Endocrinologista</h2>

						<div class="especialidades__info">
							<p class="specialty__name">Drª Mariana Valente</p>
						</div>

						<p class="especialidades__text">
							Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
						</p>

						<p class="specialty__name mb-30">Formação Acadêmica</p>
						<p class="specialty__school">MEDICINA - UFSC</p>
						<p class="specialty__school">PÓS-GRADUAÇÃO EM DERMATOLOGIA - UFSC</p>
					</div>

				</div>

			</div>
		</div>
	</section>

	</main>

	<?php require_once('../components/footer.php'); ?>