<?php require_once('../components/header-interna.php'); ?>

<main class="page__contato" role="main">

	<section class="block__contato">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<h2 class="section__title">Contato</h2>
					<p class="section__description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>

					<form name="contactForm" class="contact__form" method="POST">
						<div class="form__fields">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6">

									<label for="nome" aria-labelledby="nome">
										<input type="text" name="nome" placeholder="Nome">
									</label>

									<label for="email" aria-labelledby="email">
										<input type="email" name="email" placeholder="E-mail">
									</label>

									<label for="telefone" aria-labelledby="telefone">
										<input type="tel" name="telefone" placeholder="Telefone">
									</label>

									<div class="contato__infos-item">
										<i class="fa fa-map-marker fa-flip-horizontal"></i>
										<p>
											Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
										</p>
									</div>

									<div class="contato__infos-item">
										<i class="fa fa-phone fa-flip-horizontal"></i>
										<a href="tel:+5548991045122">
											48-991045122
										</a>
									</div>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-6">
									<label for="mensagem" aria-labelledby="mensagem">
										<textarea name="mensagem" placeholder="Mensagem"></textarea>
									</label>

									<div class="form__actions">
										<input class="btn btn-submit pull-right" type="submit" value="Enviar">
									</div>
								</div>
							</div>
						</div>	
					</form>
				</div>
			</div>
		</div>
	</section>

	</main>

	<?php require_once('../components/footer.php'); ?>