<?php require_once('../components/header-interna.php'); ?>

<main role="main">

	<section class="blog">

		<div class="blog__content">

			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-6">

						<h2 class="section__title">Blog</h2>
						<p class="section__description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>

						<div class="blog__listing">
							<div class="row">
								<div class="col-xs-12">

									<article class="blog__post">
										<div class="post__details">

											<figure class="post__thumbnail">
												<img src="../assets/images/blog/blog-home.png" title="" alt="">
											</figure>

											<div class="post__info-wrapper">

												<div class="post__info">
													<h3 class="post__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh</h3>
													<div class="post__excerpt">
														<p>
															Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. 
														</p>
															
														<p>
															Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
														</p>
													</div>
												</div>
											</div>
										</div>
									</article>

									<div class="blog__share">
										<p>Compartilhar:</p>

										<ul>
											<li>
												<a href=""><i class="fa fa-facebook"></i></a>
											</li>
											<li>
												<a href=""><i class="fa fa-twitter"></i></a>
											</li>
											<li>
												<a href=""><i class="fa fa-whatsapp"></i></a>
											</li>
										</ul>
									</div>

									<div class="blog__related">
										<h3 class="blog__related__title">Posts relacionados</h3>

										<div class="row">
											<?php for ($i=1; $i <= 2; $i++): ?>
												<div class="col-xs-12 col-md-6">
													<article class="blog__post">
														<div class="post__info" >
															<a class="post__thumbnail" href="blog_interna.php" title=""><img src="../assets/images/home/blog-home.png" title="" alt=""></a>
														</div>

														<div class="post__details">
															<a href="blog_interna.php" title="Leia mais" >
																<h3 class="post__title blog__related__title-post">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh</h3>
															</a>

															<div class="section__actions post__actions">
																<a href="blog_interna.php" title="Leia mais" class="btn-read-more pull-right">
																	Continuar lendo...
																</a>
															</div>
														</div>
													</article>
												</div>
											<?php endfor; ?>
										</div>
									</div>

									<div class="blog__comments">
										<p>Deixe seu comentário:</p>

										<div class="blog__warning">
											<strong>Atenção:</strong> Os comentários abaixo são de inteira responsabilidade de seus respectivos autores e não representam, necessariamente, a opinião da Clínica Harmoniza.
										</div>

										<div class="blog__comment">
											<!-- plugin comentário WP  -->
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>

					<div class="col-xs-12 col-md-4 col-md-offset-2">

						<div class="aside__group">
							<aside class="block__search">
								<form class="search__form blog__search" name="" action="">
									<label for="search" aria-labelledby="search">
										<input type="text" name="search" placeholder="Pesquisar">
									</label>
									<button type="submit" class="btn-search">
										<span class="screen-readers">Pesquisar</span>
									</button>
								</form>
							</aside>

							<aside class="block__newsletter">
								<h2 class="book__title">Cadatre seu e-mail para receber as dicas e novidades</h2>

								<form name="contactForm" class="book__form contact__form" method="POST">
									<div class="form__fields">
										<label for="nome" aria-labelledby="nome">
											<input type="text" name="nome" placeholder="Nome">
										</label>

										<label for="email" aria-labelledby="email">
											<input type="email" name="email" placeholder="E-mail">
										</label>

										<div class="form__actions">
											<input class="btn btn-submit" type="submit" value="eu quero receber">
										</div>
									</div>	
								</form>
							</aside>

							<aside class="block__most-readed block__aside">
								<h3>Posts mais lidos</h3>

								<div class="most-readed__listing">
									<?php for ($i=1; $i <= 2; $i++): ?>
										<article class="most-readed__list-item">

											<div class="post__details">
												<figure class="post__thumbnail">
													<img src="../assets/images/blog/blog-aside.png" title="" alt="">
												</figure>

												<h4 class="post__title"><a href="" title="">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh</a></h4>

												<a href="blog_interna.php" title="Leia mais" class="btn-read-more pull-right">Continuar Lendo...</a>
											</div>

										</article>	
									<?php endfor; ?>
								</div>
							</aside>

							<aside class="block__categories block__aside">
								<h3>Categorias</h3>

								<ul class="category__listing">
									<?php for ($i=1; $i <= 4; $i++): ?> 
										<li>
											<h4>
												<a href="categoria_interna.php" title="">Categoria</a>
											</h4>
										</li>
									<?php endfor; ?>
								</ul>
							</aside>
						</div>	

					</div>

				</div>

			</div>

		</div>
	</section>

</main>

<?php require_once('../components/footer.php'); ?>