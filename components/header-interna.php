<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('head.php'); ?>
</head>
<body>

  <header class="main__header main__header--interna">
    <div class="container">
      <div class="row align-flex">

        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg ">
          <a href="javascript:void(0)" class="btn-mobile js-mobile-menu hidden-md hidden-lg">
            <span></span>
          </a>

        </div>

        <div class="col-xs-8 col-sm-8 col-md-2 text-center">
          <figure class="header__logo">
            <a href="" title="">
              <img src="../assets/images/logo_cor.png" title="Página Inicial" alt="Página Inicial">
            </a>
          </figure>
        </div>   

        <div class="col-xs-12 col-md-9 col-md-offset-3 hidden-sm">
          <ul class="social__listing pull-right hidden-xs hidden-sm">
            <li>
              <a href="" title="Clínica Harmoniza no Facebook">
                <i class="fa fa-facebook"></i>
              </a>
            </li>
            <li>
              <a href="" title="Clínica Harmoniza no Instagram">
                <i class="fa fa-instagram"></i>
              </a>
            </li>
            <li>
              <a href="" title="Clínica Harmoniza no Google Plus">
                <i class="fa fa-google-plus"></i>
              </a>
            </li>
            <li>
              <a href="" title="Clínica Harmoniza no Youtube">
                <i class="fa fa-youtube-play"></i>
              </a>
            </li>
          </ul>
          <ul class="header__nav">
           <li><a href="especialidades.php" title="Especialidades">Especialidades</a></li>
           <li><a href="clinica.php" title="A Clínica">A Clínica</a></li>
           <li><a href="blog.php" title="Blog">Blog</a></li>
           <li><a href="localizacao.php" title="Localização">Localização</a></li>
           <li><a href="contato.php" title="Contato">Contato</a></li>
         </ul>
       </div>    

     </div>
   </div>
 </header>