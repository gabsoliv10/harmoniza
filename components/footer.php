<footer class="main__footer">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-4">

				<div class="block__about">
					<a class="footer__logo" href="" title="">
						<img src="../assets/images/logo-negative.png" title="Clínica Harmoniza" alt="Clínica Harmoniza">
					</a>

					<div class="footer__description">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa repellat doloremque excepturi ut totam sint illum.
						</p>

						<ul class="social__listing">
							<li>
								<a href="" title="Clínica Harmoniza no Facebook">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a href="" title="Clínica Harmoniza no Instagran">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
							<li>
								<a href="" title="Clínica Harmoniza no Google Plus">
									<i class="fa fa-google-plus"></i>
								</a>
							</li>
							<li>
								<a href="" title="Clínica Harmoniza no Youtube">
									<i class="fa fa-youtube-play"></i>
								</a>
							</li>
						</ul>

						<div class="footer__responsible">
							<p>Responsável Técnica</p>
							<p>Drª Camila Nemoto de Mendonça</p>
							<p>CREMESC 15880/ RQE 12052</p>
						</div>
					</div>
				</div>			
			</div>
			
			<div class="col-xs-12 col-md-4">
				<div class="footer__instagram">
					<h2 class="footer__title">Instagram</h2>
				</div>
			</div>

			<div class="col-xs-12 col-md-3 pull-right">
				<div class="footer__facebook pull-right">
					<img src="../assets/images/facebook.png" title="" alt="">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-xs-12">
				<div class="footer__copyright">
					<figure class="code__logo pull-right">
						<a href="http://codde.com.br/" title="Desenvolvido por Code"><img src="../assets/images/logo-codde.png" alt=""></a>
					</figure>
				</div>
			</div>
		</div>
	</div>
</footer>

<script async defer src="https://use.fontawesome.com/4d634eeb4a.js"></script>

<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="../assets/js/owl.carousel.min.js"></script>
<script src="../assets/js/app.js"></script>

</body>
</html>